<!DOCTYPE html>
<html>

    <head>
        <title>Form</title>
        <meta charset="UTF-8">
    </head>

    <body>
        <h2>Buat Account Baru!</h2>
        <h4>Sign Up Form</h4>

            <form action="/welcome" method="get">
                <label for="First Name">First name:</label> <br><br>
                <Input type="text" id="First Name">
                
                <br>
                <br>

                <label for="Last Name">Last name:</label> <br><br>
                <Input type="text" id="Last Name">

                <br>
                <br>
                
                <label>Gender:</label> <br><br>
                <Input type="radio" name="Gender" Value="Male"> Male <br>
                <Input type="radio" name="Gender" Value="Female"> Female <br>
                <Input type="radio" name="Gender" Value="Other"> Other <br>

                <br>

                <label>Nationality:</label> <br><br>
                <select name="Nationality">
                    <option value="Indonesian">Indonesian</option>
                    <option value="Singaporian">Singaporian</option>
                    <option value="Malaysian">Malaysian</option>
                    <option value="Australian">Australian</option>
                </select>
                
                <br>
                <br>

                <label>Language Spoken:</label> <br><br>
                <input type="checkbox" name="Bahasa" value="Indonesia">Bahasa Indonesia<br>
                <input type="checkbox" name="Bahasa" value="English">English<br>
                <input type="checkbox" name="Bahasa" value="Other">Other

                <br>
                <br>

                <label>Bio:</label><br><br>
                <textarea name="Bio" cols="30" rows="10"></textarea>

                <br>

                <input type="submit" value="Sign Up">
            </form>
    </body>

</html>